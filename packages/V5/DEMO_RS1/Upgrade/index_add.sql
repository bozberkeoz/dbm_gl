SELECT 'Index is being created!' as msg;
CREATE INDEX test1_indx 
ON test1(col1);
SELECT 'Index has been created!' as msg;